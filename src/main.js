import SectionCreator from './join-us-section.js';
import loadCommunity from './loadCommunity.js';
import './styles/style.css';


window.addEventListener('load', () => {
  loadCommunity();
  const creator = new SectionCreator();
  const newSection = creator.create('standard'); // eslint-disable-line no-unused-vars
  // newSection.remove();
});
