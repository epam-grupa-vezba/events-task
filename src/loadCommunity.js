class CommunityCard {
  constructor(backgroundImg, description, userName, company, communityContainer){ // eslint-disable-line
    this.backgroundImg = backgroundImg;
    this.userName = userName;
    this.description = description;
    this.company = company;
    this.parent = communityContainer;
  }
  
  create(){
    const communityCard = document.createElement('div');
    this.parent.appendChild(communityCard);
    communityCard.className = 'app-communityCard';
    
    const communityImg = document.createElement('img');
    communityCard.appendChild(communityImg);
    communityImg.className = 'app-cardImg';
    communityImg.src = this.backgroundImg;

    const description = document.createElement('p');
    communityCard.appendChild(description);
    description.className = 'app-communityDescription';
    description.innerHTML = this.description;

    const userName = document.createElement('p');
    communityCard.appendChild(userName);
    userName.className = 'app-communityUserName';
    userName.innerHTML = this.userName;

    const company = document.createElement('p');
    communityCard.appendChild(company);
    company.className = 'app-communityCompany';
    company.innerHTML = this.company;

    return communityCard;
  }
}

const loadCommunity = () => {
  const parent = document.getElementById('app-container');

  const communitySection = document.createElement('section');
  communitySection.className = 'app-section app-section--communitySection';

  const beforeCommunity = document.getElementById('culture');
  parent.insertBefore(communitySection, beforeCommunity);

  const communityHeader = document.createElement('h2');
  communityHeader.className = 'app-title app-communityTitle';
  communityHeader.innerHTML = 'Big Community of <br/> People Like You';

  communitySection.appendChild(communityHeader);

  const subText = document.createElement('h3');
  subText.className = 'app-subtitle app-communitySub';
  subText.innerHTML = 'We’re proud of our products, and we’re really excited <br/> when we get feedback from our users.';

  communitySection.appendChild(subText);

  const communityContainer = document.createElement('div');
  communityContainer.className = 'app-communityContainer';
  communitySection.appendChild(communityContainer);

  fetch('http://localhost:9000/api/community')
    .then((response) => response.json())
    .then((data) => {
      let descriptionTexts = [
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolor.',
        'Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut.',
        'Aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.',
      ];
      data.map((x, i) => {
        const communityCard1 = new CommunityCard(`${x.avatar}`, `${descriptionTexts[i]}`, `${x.firstName} ${x.lastName}`, `${x.position}`, communityContainer);  
        communityCard1.create();
        console.log(x);
        return x;
      });
    });
};

export default loadCommunity;
