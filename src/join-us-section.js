import validate from './email-validator';

class Section {
  constructor(title, submitText) {
    this.title = title;
    this.submitText = submitText;
    this.element = this.create();
  }

  create() {
    const parent = document.getElementById('app-container');

    const section = document.createElement('section');
    section.className = 'app-section app-section--joinOurProgram';

    const beforeNode = document.getElementById('footer');
    parent.insertBefore(section, beforeNode);

    const heading = document.createElement('h2');
    heading.innerHTML = this.title;
    heading.className = 'app-title app-title--join';
    section.appendChild(heading);

    const text = document.createElement('h3');
    text.innerHTML = 'Sed do eiusmod tempor incididunt <br> ut labore et dolore magna aliqua.';
    text.className = 'app-subtitle app-subtitle--join';
    section.appendChild(text);

    const formContainer = document.createElement('form');
    formContainer.className = 'app-formContainer';
    formContainer.id = 'myForm';
    section.appendChild(formContainer);

    const inputField = document.createElement('input');
    inputField.type = 'email';
    inputField.placeholder = 'Email';
    inputField.name = 'userInput';
    inputField.className = 'app-inputField';
    formContainer.appendChild(inputField);

    const button = document.createElement('button');
    button.type = 'submit';
    button.innerHTML = this.submitText;
    button.className = 'app-section__button app-section__button--join';
    formContainer.appendChild(button);
  
    inputField.value = localStorage.getItem('email');

    if (localStorage.getItem('isValidInput')) {
      inputField.style.display = 'none';
      button.innerHTML = 'Unsubscribe';
    } else {
      inputField.style.display = 'inline';
      button.innerHTML = 'Subscribe';
    }

    inputField.addEventListener('input', (event) => {
      localStorage.setItem('email', event.target.value);
    });

    formContainer.addEventListener('submit', async (event) => {
      event.preventDefault();
      const userEmail = document.querySelector('.app-inputField').value;

      const isValid = validate(userEmail);

      if (isValid && button.innerHTML !== 'Unsubscribe') {
        localStorage.setItem('isValidInput', isValid);
        
        const bodyData = {
          email: userEmail,
        };

        button.disabled = true;
        button.style.opacity = 0.5;

        fetch('http://localhost:9000/api/subscribe', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(bodyData),
        })
          .then((response) => response.json())
          .then((data) => {
            button.disabled = false;
            button.style.opacity = 1;
            if(data.error) {
              throw new Error(data.error);
            } else {
              inputField.style.display = 'none';
              button.innerHTML = 'Unsubscribe';
            }
          })
          .catch((error) => {
            window.alert(error);
            localStorage.removeItem('isValidInput');
            localStorage.removeItem('email');
          });
      } else if(button.innerHTML === 'Unsubscribe') {
        button.disabled = true;
        button.style.opacity = 0.5;

        fetch('http://localhost:9000/api/unsubscribe', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
        })
          .then((response) => response.json())
          .then((data) => {
            button.disabled = false;
            button.style.opacity = 1;
            if(data.success){
              localStorage.removeItem('isValidInput');
              localStorage.removeItem('email');
              inputField.style.display = 'inline';
              inputField.value = '';
              button.innerHTML = 'Subscribe';
            }
          });  
      } else {
        window.alert('Please enter valid email.');
      }
    });

    return section;
  }

  remove() {
    this.element.remove();
  }
}

class SectionCreator {
  create(type) {
    switch(type) {
      case 'standard':
        return new Section('Join Our Program', 'Subscribe');
      case 'advanced':
        return new Section('Join Our Advanced Program', 'Subscribe to Advanced Program');
      default:
        return null;
    }
  }
}

export default SectionCreator;
