const VALID_EMAIL_ENDINGS = ['gmail.com', 'outlook.com', 'yandex.ru'];

const validate = (email) => {
  const splitEmail = email.split('@');
  const emailEnd = splitEmail[1];
  if(splitEmail.length !== 2) {
    return false;
  }

  if(splitEmail[0].length === 0) {
    return false;
  }

  const isEmailValid = VALID_EMAIL_ENDINGS.includes(emailEnd);
  return isEmailValid;
};

export default validate;
