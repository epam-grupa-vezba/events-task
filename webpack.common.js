const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path') ;
const CopyPlugin = require("copy-webpack-plugin");
const ESLintPlugin = require("eslint-webpack-plugin");

module.exports = {
  entry: './src/main.js',
  output: {
    filename: 'main.js',
    path: path.resolve(__dirname, 'build'),
  },

  plugins: [
    new CopyPlugin({
      patterns: [
        { from: './src/assets/images', to: '.' },
      ],
    }),

    new ESLintPlugin(),

    new HtmlWebpackPlugin({
      template: './src/index.html',
    })],

  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          { loader: 'style-loader' },
          { loader: 'css-loader' },
        ],
      },
    ],
  },
};
