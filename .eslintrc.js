module.exports = {
  env: {
    browser: true,
    es2021: true,
  },
  extends: 'airbnb-base',
  overrides: [
    {
      env: {
        node: true,
      },
      files: [
        '.eslintrc.{js,cjs}',
      ],
      parserOptions: {
        sourceType: 'script',
      },
    },
  ],
  parserOptions: {
    ecmaVersion: 'latest',
    sourceType: 'module',
  },
  rules: {
    "linebreak-style" : 0,
    "space-before-blocks" : 1,
    "no-multiple-empty-lines" : 0,
    "keyword-spacing" : 0,
    "prefer-const" : 1,
    "max-classes-per-file" : 0,
    "import/extensions" : 0,
    "no-useless-concat" : 0,
    "class-methods-use-this" : 0,
    "consistent-return" : 1,
    "no-trailing-spaces" : 0
    },
};
